import "./App.css";
import React, { useState, useRef } from "react";

function App() {
  const [todoList, settodoList] = useState([]);
  const [currentTask, setcurrentTask] = useState("");
  const inputTask = useRef(null);

  const addTask = () => {
    settodoList([...todoList, { task: currentTask, completed: false }]);
    inputTask.current.value = "";
    setcurrentTask("");
  };

  const deleteTask = (todelete) => {
    settodoList(
      todoList.filter((allTask) => {
        return allTask.task !== todelete;
      })
    );
  };
  const completedTask = (big) => {
    settodoList(
      todoList.map((allTask) => {
        return allTask.task === big
          ? { task: big, completed: true }
          : {
              task: allTask.task,
              completed: allTask.completed === true ? true : false,
            };
      })
    );
  };

  return (
    <div className="App">
      <h1>To Do List</h1>
      <div>
        <input
          ref={inputTask}
          type="text"
          onKeyDown={(event) => {
            event.keyCode === 13 && addTask();
          }}
          placeholder="task......"
          onChange={(event) => setcurrentTask(event.target.value)}
        />
        <button onClick={addTask}>Add Task</button>
      </div>
      <hr />
      <ul>
        {todoList.map((val, key) => {
          return (
            <div id="task">
              <li key={key}>{val.task}</li>
              <button onClick={() => completedTask(val.task)}>Completed</button>
              <button onClick={() => deleteTask(val.task)}>Delete</button>
              {val.completed ? (
                <h2>Task Completed</h2>
              ) : (
                <h2>Task Not Completed</h2>
              )}
            </div>
          );
        })}
      </ul>
    </div>
  );
}

export default App;
